var gulp = require( "gulp" ),
    sass = require( "gulp-sass" ),
    cleanCSS = require( "gulp-clean-css" ),
    concatCSS = require( "gulp-concat-css" ),
    autoprefixer = require( "gulp-autoprefixer" )

// Compile and concatenate all .scss files to .css
gulp.task( "scss", function() {
  gulp.src( "static/styles/scss/*.scss" )
      .pipe( sass( { outputStyle: "compressed" } ) )
      .pipe( cleanCSS() )
      .pipe( autoprefixer( "last 10 versions" ) )
      .pipe( concatCSS( "main.css" ) )
      .pipe( gulp.dest( "static/styles" ) ) } );

// Compile and concatenate all .scss files to .css in case
// the packages was loaded as a submodule
gulp.task( "scss-submodule", function() {
  gulp.src( "static/styles/scss/*.scss" )
      .pipe( sass( { outputStyle: "compressed" } ) )
      .pipe( cleanCSS() )
      .pipe( autoprefixer( "last 10 versions" ) )
      .pipe( concatCSS( "main.css" ) )
      .pipe( gulp.dest( "../../static/styles" ) ) } );

// Watch for changes of the scss files
gulp.task( "watch", [ "scss-submodule", "scss" ], function() {
  gulp.watch( "static/styles/scss/*", [ "scss" ] ) } );


// Set the default task
gulp.task( "default", [ "watch" ] );
